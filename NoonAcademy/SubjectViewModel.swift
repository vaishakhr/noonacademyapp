//
//  SubjectViewModel.swift
//  NoonAcademy
//
//  Created by Vaishakh on 5/31/17.
//  Copyright © 2017 Vaishakh. All rights reserved.
//

import UIKit

class SubjectViewModel: CellRepresentable {
    var rowHeight: CGFloat = 90
    var subject: SubjectDataModel
    
    func cellInstance(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SubjectTableViewCell", for: indexPath) as! SubjectTableViewCell
        cell.setupTableCellModel(vm: self)
        return cell
    }
    
    init(subject: SubjectDataModel) {
        self.subject = subject
    }
    
}
