//
//  ViewController.swift
//  NoonAcademy
//
//  Created by Vaishakh on 5/31/17.
//  Copyright © 2017 Vaishakh. All rights reserved.
//

import UIKit

class ViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func initTableViewCell() {
        tableView.register(UINib.init(nibName: "SubjectTableViewCell", bundle: nil), forCellReuseIdentifier: "SubjectTableViewCell")
    }
    
    override func loadData() {
        data = [SubjectViewModel.init(subject: SubjectDataModel.init(subjectName: "Hello", subjectDescription: "Hello how are youy. how is life over there . Howare people in my country , good day , good people ", coverImage: nil))]
        tableView.estimatedRowHeight = 60
        tableView.reloadData()
    }
    
}

