//
//  SubjectTableViewCell.swift
//  NoonAcademy
//
//  Created by Vaishakh on 5/31/17.
//  Copyright © 2017 Vaishakh. All rights reserved.
//

import UIKit

class SubjectTableViewCell: UITableViewCell {
    @IBOutlet weak var bookname: UILabel!
    @IBOutlet weak var bookdescription: UILabel!

    @IBOutlet weak var bookCoverImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupTableCellModel(vm: SubjectViewModel) {
        //self.titleLabel.text = vm.subject.subjectName
        self.bookname.text = vm.subject.subjectName
        self.bookdescription.text = vm.subject.subjectDescription
        guard  let coverImageData = vm.subject.subjectCoverImage else {
            bookCoverImage = UIImageView.init(image: UIImage.init())
            return
        }
        let coverUIImage = UIImage.init(data: coverImageData as Data)
        self.bookCoverImage = UIImageView.init(image: coverUIImage)
        
    }
    
}
