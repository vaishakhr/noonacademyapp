//
//  Subject.swift
//  NoonAcademy
//
//  Created by Vaishakh on 5/31/17.
//  Copyright © 2017 Vaishakh. All rights reserved.
//

import Foundation


class SubjectDataModel : NSObject {
    var subjectName: String
    var subjectDescription: String
    var subjectCoverImage: NSData?
    
    init(subjectName: String, subjectDescription: String, coverImage: NSData?) {
        self.subjectName = subjectName
        self.subjectDescription = subjectDescription
        self.subjectCoverImage = coverImage
        super.init()
    }
}
