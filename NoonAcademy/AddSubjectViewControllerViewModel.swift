//
//  AddSubjectViewControllerViewModel.swift
//  NoonAcademy
//
//  Created by Vaishakh on 6/2/17.
//  Copyright © 2017 Vaishakh. All rights reserved.
//

import UIKit

protocol AddSubjectViewControllerViewModel {
    weak var delegate: AddSubjectViewControllerViewModelDelegate? {get set}
    var bookNamePlaceHolderText: String? {get}
    var bookNamePlaceBorderColor: UIColor? {get}
    var bookDescriptionPlaceHolderText: String? {get}
    var coverImageData: NSData? {get}
    var submitTextColor: UIColor {get}
    var submitEnabled: Bool {get}
}

protocol AddSubjectViewControllerViewModelDelegate: class {
    func reloadViews()
    func tellDelegateToListScreen()
}
