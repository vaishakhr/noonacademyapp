//
//  AddSubjectInputValidator.swift
//  NoonAcademy
//
//  Created by Vaishakh on 6/2/17.
//  Copyright © 2017 Vaishakh. All rights reserved.
//

import UIKit

enum AddSubjectInputValidatorError: Error {
    case EmptyBookName
    case EmptyBookDescription
}

class  AddSubjectInputValidator: NSObject {

    
    func validateBookName(bookName: String?) throws {
        guard let bookName = bookName else {
            throw AddSubjectInputValidatorError.EmptyBookName
        }
    }
    
    func validateBookDescription(bookDescription: String?) throws {
        guard let bookDescription = bookDescription else {
            throw AddSubjectInputValidatorError.EmptyBookDescription
        }
    }
    
    
}
