//
//  BaseViewController.swift
//  NoonAcademy
//
//  Created by Vaishakh on 6/2/17.
//  Copyright © 2017 Vaishakh. All rights reserved.
//

import UIKit


class BaseViewController: UIViewController {
    
    
    @IBOutlet weak var tableView: UITableView!
    var data = [CellRepresentable]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        initTableViewCell()
        loadData()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Override this in the Child Class with respective Custom Cell Initializer
    func initTableViewCell() {
    
    }
    
    // Override this in the Child Class to load Data from WebService or from Database
    func loadData() {
        
    }
}


extension BaseViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return data[indexPath.row].cellInstance(tableView, indexPath: indexPath)
    }
}

extension BaseViewController: UITableViewDelegate {
 
}



