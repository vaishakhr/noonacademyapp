//
//  AddSubjectViewControllerViewModel.swift
//  NoonAcademy
//
//  Created by Vaishakh on 6/2/17.
//  Copyright © 2017 Vaishakh. All rights reserved.
//

import UIKit

class AddSubjectViewControllerDefaultViewModel: AddSubjectViewControllerViewModel {
    
    var coverImageData: NSData? = nil

    var bookDescriptionPlaceHolderText: String? = "Book Description"

    weak internal var delegate: AddSubjectViewControllerViewModelDelegate?

    var bookNamePlaceHolderText: String? = "Book Name"
    var bookNamePlaceBorderColor: UIColor? {
        return .black}
    
    var submitTextColor: UIColor {
        return submitEnabled ? .black : .lightGray
    }
    
    
    fileprivate func tellDelegateToMoveToListScreen() {
        delegate?.tellDelegateToListScreen()
    }
    
    fileprivate func tellDelegateToReloadView() {
        delegate?.reloadViews()
    }
    
    private let addSubjectInputValidator: AddSubjectInputValidator
    private var bookNameText: String = ""
    private var bookDescriptionText: String = ""
    private var bookNameValid: Bool = false
    private var bookDescriptionValid: Bool = false
    
    init(addSubjectInputValidator: AddSubjectInputValidator) {
        self.addSubjectInputValidator = addSubjectInputValidator
    }
    
    var submitEnabled: Bool {
        return bookNameValid && bookDescriptionValid
    }
    
    func submitButtonPressed (){
        do {
            try addSubjectInputValidator.validateBookDescription(bookDescription: bookDescriptionText)
            try addSubjectInputValidator.validateBookName(bookName: bookNameText)
            
        } catch {
            print("Error ")
        }
    }
    
}
